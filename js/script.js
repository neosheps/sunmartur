$(document).ready(function() {

    //Smooth Scroll
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });


    // Main Menu
    $('#main-nav').affix({
        offset: {
            top: $('header').height()
        }
    });

    $(".navbar-collapse a").on('click', function() {
        $(".navbar-collapse.collapse").removeClass('in');
    });

    // Top Search
    $("#ss").click(function(e) {
        e.preventDefault();
        $(this).toggleClass('current');
        $(".search-form").toggleClass('visible');
    });


    //Slider
    $('.flexslider').flexslider({
        animation: "fade",
        directionNav: false,
        pauseOnAction: false,
    });

    var containerPosition = $('.container').offset();
    var positionPad = containerPosition.left + 15;

    $('#slider').find('.caption').css({
        left: positionPad + 'px',
        marginTop: '-' + $('.caption').height() / 2 + 'px'
    });


    // number effect
    $('.about-bg-heading').one('inview', function(event, visible) {
        if (visible == true) {
            $('.count').each(function() {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 5000,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
    });


    //Google Map
    var get_latitude = $('#google-map').data('latitude');
    var get_longitude = $('#google-map').data('longitude');

    function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
            zoom: 14,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize_google_map);

});

$(document).ready(function(){
    $(".flexslider").css('display','block').slideUp();
});

$(document).ready(function() {

    //Accordion
    $(".close-project").click(function(){
        $(".flexslider").slideUp("slow");
    });

    $(".client").click(function() {

        var project = this.id;
        var project_id = '#' + project + '-project';
        var elem = $(".flexslider:visible").length ? $(".flexslider:visible") : $(".flexslider:first");
        elem.slideUp('slow', function () {
            $(project_id).slideDown('slow');
        });
    });

    //Flexslider
    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: false,
        prevText: "&larr;",
        nextText: "&rarr;",
        controlNav: true
    });

});

$('#collapseOne').collapse("hide");
