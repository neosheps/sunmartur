ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [55.826589, 37.456327],
            zoom: 15,
			controls: ['zoomControl']
    }, {
            searchControlProvider: 'yandex#search'
        });
		
myMap.geoObjects
        .add(new ymaps.Placemark([55.826959, 37.450177], {
            balloonContentHeader: 'Sunmar на Тушинской',
			balloonContentBody: "ул. Тушинская, д.8 ТЦ Купчино",
            iconCaption: 'Sunmar'
        }, {
			
            preset: 'plain#icon',
            iconCaptionMaxWidth: '500'
        }));
myMap.behaviors.disable('scrollZoom')
}
